package ru.savalek.filebrowser

import com.sun.net.httpserver.HttpServer
import ru.savalek.filebrowser.handler.FileLoadHandler
import ru.savalek.filebrowser.handler.PageLoadHandler
import java.net.InetAddress
import java.net.InetSocketAddress


fun main() {
    val server = HttpServer.create(InetSocketAddress(7778), 0)
    server.createContext("/", PageLoadHandler())
    server.createContext("/file/", FileLoadHandler())
    server.executor = null // creates a default executor
    server.start()
    printIp()
}

@Suppress("HttpUrlsUsage")
fun printIp() {
    println("IP Address: http://${InetAddress.getLocalHost().hostAddress}:7778")
}




