package ru.savalek.filebrowser.handler

import ru.savalek.filebrowser.Properties.Companion.FOLDER_PATH
import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpHandler
import ru.savalek.filebrowser.Html
import java.io.File
import java.nio.charset.Charset
import kotlin.streams.asStream

class PageLoadHandler : HttpHandler {

    override fun handle(t: HttpExchange) {
        try {
            println("Page load called")
            val html = Html()

            File(FOLDER_PATH).walk().asStream()
                .filter { it.isFile }
                .forEach(html::addFile)

            sendPageToUser(html, t)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun sendPageToUser(html: Html, t: HttpExchange) {
        val htmlData = html.asText().toByteArray(Charset.defaultCharset())
        t.sendResponseHeaders(200, htmlData.size.toLong())
        t.responseBody.use {
            it.write(htmlData)
        }
    }
}