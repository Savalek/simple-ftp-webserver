package ru.savalek.filebrowser.handler

import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpHandler
import java.io.File
import java.io.FileInputStream
import java.net.URLDecoder
import java.net.URLEncoder

class FileLoadHandler : HttpHandler {

    override fun handle(exchange: HttpExchange) {
        try {
            val file = getFile(exchange)
            println("File load started: '${file.name}'")
            if (file.exists() && file.isFile) {
                addHeaders(exchange, file)
                FileInputStream(file).use { inputStream ->
                    exchange.responseBody.use { output ->
                        val buffer = ByteArray(65536)
                        var bytesRead: Int
                        while (inputStream.read(buffer).also { bytesRead = it } != -1) {
                            output.write(buffer, 0, bytesRead)
                        }
                    }
                }
                println("File load finished: '${file.name}'")
            } else {
                exchange.sendResponseHeaders(404, 0)
                println("File not found: '${file.name}'")
            }
            exchange.close()
        } catch (e: Exception) {
            println("File load error: ${e.message}")
            e.printStackTrace()
        }
    }

    private fun addHeaders(exchange: HttpExchange, file: File) {
        val filename = URLEncoder.encode(file.name, "UTF-8")
        exchange.run {
            responseHeaders.set("Content-Disposition", "attachment; filename*=UTF-8''\"$filename\"")
            responseHeaders.set("Content-Type", "application/octet-stream")
            sendResponseHeaders(200, file.length())
        }
    }

    private fun getFile(exchange: HttpExchange): File {
        var filePath = exchange.requestURI.rawPath.split("file/").last()
        filePath = URLDecoder.decode(filePath, "UTF-8")
        return File(filePath)
    }
}