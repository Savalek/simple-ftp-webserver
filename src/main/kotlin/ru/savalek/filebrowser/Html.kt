package ru.savalek.filebrowser

import ru.savalek.filebrowser.Properties.Companion.FOLDER_PATH
import java.io.File
import java.net.URLDecoder
import java.net.URLEncoder

class Html {

    private var html: String = this::class.java.getResource("/page.html")!!.readText()
    private val files: ArrayList<File> = ArrayList()

    fun addFile(file: File) {
        files.add(file)
    }

    fun asText(): String {
        var text = html
        text = text.replace("<!--files-->", filesAsHtmlList())
        return text
    }

    private fun filesAsHtmlList() = files.joinToString(System.lineSeparator()) {
        val decodeFilePath = URLEncoder.encode(it.path, "UTF-8")
        "<li><a href='/file/$decodeFilePath'>${it.path.replace(FOLDER_PATH, "")}</a></li>"
    }
}